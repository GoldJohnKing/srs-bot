﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace RurouniJones.OverlordBot.SimpleRadio.Models
{
    public class RadioInformation
    {
        [JsonProperty(PropertyName = "radios")]
        public List<Radio> Radios = new();
        
        [JsonProperty(PropertyName = "unit")]
        public string Unit = "External AWACS";
        
        [JsonProperty(PropertyName = "unitId")]
        public uint UnitId = 100000001;

        public class Radio
        {
            [SuppressMessage("ReSharper", "UnusedMember.Global")]
            public enum Modulations
            {
                Am = 0,
                Fm = 1,
                Intercom = 2,
                Disabled = 3,
                Havequick = 4, //unsupported currently
                Satcom = 5, //unsupported currently
                Mids = 6
            }

            [JsonProperty(PropertyName = "enc")]
            public bool IsEncrypted;

            [JsonProperty(PropertyName = "encKey")]
            public byte EncryptionKey;

            [JsonProperty(PropertyName = "freq")]
            public double Frequency = 1;

            [JsonProperty(PropertyName = "modulation")]
            public Modulations Modulation = Modulations.Disabled;
        }
    }
}