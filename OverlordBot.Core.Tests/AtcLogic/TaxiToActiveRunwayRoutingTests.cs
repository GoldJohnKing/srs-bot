﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using Geo.Geometries;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Core.Atc.Util;
using RurouniJones.OverlordBot.Encyclopedia;

namespace RurouniJones.OverlordBot.Core.Tests.AtcLogic
{
    [TestClass]
    public class TaxiToActiveRunwayRoutingTests
    {
        public static IEnumerable<object[]> TaxiScenarios()
        {
            // ReSharper disable StringLiteralTypo
            yield return new object[] { new TaxiScenario("Al Dhafra AFB", "Ramp 1", 310, new Point(24.244862290776, 54.562445823216), "Runway-3 1 Right", new List<string> { "Alfa" }) };
            yield return new object[] { new TaxiScenario("Al Dhafra AFB", "Ramp 1", 170, new Point(24.244862290776, 54.562445823216), "Runway-1 3 Left", new List<string> { "Alfa", "Foxtrot" }) };

            yield return new object[] { new TaxiScenario("Al Dhafra AFB", "Uniform Spots 1", 310, new Point(24.216944646008, 54.563235561423), "Runway-3 1 Left", new List<string> { "Uniform", "Uniform 0" }) };
            yield return new object[] { new TaxiScenario("Al Dhafra AFB", "Uniform Spots 1", 10, new Point(24.216944646008, 54.563235561423), "Runway-3 1 Left", new List<string> { "Uniform", "Uniform 0" }) };
            
            yield return new object[] { new TaxiScenario("Anapa-Vityazevo", "Apron 1", 90, new Point(45.0101581, 37.3481765), "Runway-0 4", new List<string> { "Mike", "Alpha" }) };
            yield return new object[] { new TaxiScenario("Anapa-Vityazevo", "Echo Spots", 90, new Point(45.0094606, 37.3635130), "Runway-0 4", new List<string> { "Echo", "Delta", "Mike", "Alpha" }) };

            yield return new object[] { new TaxiScenario("Anapa-Vityazevo", "Apron 1", -1, new Point(45.0101581, 37.3481765), "Runway-0 4", new List<string> { "Mike", "Alpha" }) };
            yield return new object[] { new TaxiScenario("Anapa-Vityazevo", "Apron 1", 350, new Point(45.0101581, 37.3481765), "Runway-0 4", new List<string> { "Mike", "Alpha" }) };
            yield return new object[] { new TaxiScenario("Anapa-Vityazevo", "Apron 1", 10, new Point(45.0101581, 37.3481765), "Runway-0 4", new List<string> { "Mike", "Alpha" }) };

            yield return new object[] { new TaxiScenario("Anapa-Vityazevo", "Apron 1", 270, new Point(45.0101581, 37.3481765), "Runway-2 2", new List<string> { "Mike", "Delta" }) };
            yield return new object[] { new TaxiScenario("Anapa-Vityazevo", "Echo Spots", 270, new Point(45.0094606, 37.3635130), "Runway-2 2", new List<string> { "Echo" }) };
            yield return new object[] { new TaxiScenario("Anapa-Vityazevo", "Echo Spots", 40, new Point(45.0094606, 37.3635130), "Runway-0 4", new List<string> { "Echo", "Delta", "Mike", "Alpha" }) };
            
            yield return new object[] { new TaxiScenario("Krasnodar-Center", "Echo Spots 1", 340, new Point(45.082339143765, 38.954220071576), "Runway-2 7", new List<string> { "Echo" }) };
            yield return new object[] { new TaxiScenario("Krasnodar-Center", "Echo Spots 1", 270, new Point(45.082339143765, 38.954220071576), "Runway-2 7", new List<string> { "Echo" }) };

            yield return new object[] { new TaxiScenario("Krasnodar-Pashkovsky", "Apron A", 90, new Point(45.038881971882, 39.14140614585), "Runway-0 5 Left", new List<string> { "Alfa" }) };
            yield return new object[] { new TaxiScenario("Krasnodar-Pashkovsky", "Apron 1", 90, new Point(45.047601257612, 39.200777416505), "Runway-0 5 Right", new List<string> { "November", "Echo" }) };

            yield return new object[] { new TaxiScenario("Mineralnye Vody", "Maintenance Area", 90, new Point(44.223130773545, 43.104834499253), "Runway-1 2", new List<string> { "November", "Echo", "Alfa" }) };

            yield return new object[] { new TaxiScenario("Maykop-Khanskaya", "Apron 2 1", 270, new Point(44.671288619807, 40.030771437314), "Runway-2 2", new List<string> { "Victor", "Echo" }) };
            // ReSharper restore StringLiteralTypo
        }

        private readonly HashSet<AirfieldStatus> _airfields = PopulateAirfields();

        private static HashSet<AirfieldStatus> PopulateAirfields()
        {
            var airfields = new HashSet<AirfieldStatus>();
            foreach (var airfield in Airfield.Entries)
            {
                airfields.Add(new AirfieldStatus(airfield, "TEST"));
            }

            return airfields;
        }

        [DataTestMethod]
        [DynamicData(nameof(TaxiScenarios), DynamicDataSourceType.Method)]
        public void TaxiToActiveRunway(TaxiScenario scenario)
        {
            var airfield = _airfields.First(af => af.Name.ToLower().Equals(scenario.Airfield.ToLower()));

            airfield.WindHeading = scenario.Wind;
            var expected = new TaxiRouter.TaxiInstructions
            {
                DestinationName = scenario.Destination,
                TaxiwayNames = scenario.Taxiways,
                Comments = new List<string>()
            };

            var actual = TaxiRouter.Process(scenario.StartPoint, airfield);

            AssertInstructions(expected, actual);
        }

        private static void AssertInstructions(TaxiRouter.TaxiInstructions expected, TaxiRouter.TaxiInstructions actual)
        {
            Console.WriteLine(expected);
            Console.WriteLine(actual);
            StringAssert.Contains(expected.DestinationName, actual.DestinationName);
            CollectionAssert.AreEqual(expected.TaxiwayNames, actual.TaxiwayNames);
        }
        public class TaxiScenario
        {
            public string Airfield { get; }
            public string Source { get; }
            public int Wind { get; }
            public Point StartPoint { get; }
            public string Destination { get; }
            public List<string> Taxiways { get; }
            public List<string> Comments { get; } = new();

            public TaxiScenario(string airfield, string source, int wind, Point startPoint, string destination, List<string> taxiways)
            {
                Airfield = airfield;
                Source = source;
                Wind = wind;
                StartPoint = startPoint;
                Destination = destination;
                Taxiways = taxiways;
            }

            public override string ToString()
            {
                return $"{Airfield} (Wind {Wind:D3}): {Source} -> {Destination}";
            }
        }
    }
}
