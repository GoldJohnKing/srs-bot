﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NAudio.Wave;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Cognitive.SpeechOutput;
using RurouniJones.OverlordBot.Cognitive.SpeechRecognition;
using RurouniJones.OverlordBot.Core.Awacs.IntentHandlers;
using RurouniJones.OverlordBot.Core.Models;
using RurouniJones.OverlordBot.Core.Monitors;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Diagnostics;
using RurouniJones.OverlordBot.Discord;
using RurouniJones.OverlordBot.SimpleRadio.Audio;
using Client = RurouniJones.OverlordBot.SimpleRadio.Client;

namespace RurouniJones.OverlordBot.Core
{
    public class RadioController
    {
        public static ConcurrentDictionary<Guid, bool> ControllerEnabled = new();

        public delegate void QueueTransmissionDelegate(Reply result);
        public delegate Task LogTransmissionDelegate(string text);

        //https://trac.ffmpeg.org/wiki/audio%20types
        private readonly BufferedWaveProvider _audioBuffer = new(new WaveFormat(16000, 1))
        {
            BufferDuration = new TimeSpan(0, 0, 30),
            DiscardOnBufferOverflow = true,
            ReadFully = false
        };
        private readonly string _serverShortName;
        private readonly Configuration.ControllerConfiguration _controllerConfig;
        private readonly ulong _discordLogServerId;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly Configuration.SimpleRadioConfiguration _simpleRadioConfig;

        private readonly ConcurrentQueue<Queue<(byte[], TimeSpan)>> _transmissionQueue = new();
        private Client _client;

        private CancellationTokenSource _controllerCancellationTokenSource;

        private DateTime _lastTransmission;

        public ConcurrentDictionary<string, string> PlayersOnFrequency = new();

        /// <summary>
        ///     Implements the logic for background monitoring tasks that may result
        ///     in transmissions being sent to players.
        /// </summary>
        private IMonitor _monitor;

        /// <summary>
        ///     Implements the logic for request / reply handling from transmitters
        /// </summary>
        private IResponder _responder;
        
        private bool _receivingTransmission = false;
        private readonly List<Reply> _messageQueue = new();

        public Guid Id = Guid.NewGuid();

        public RadioController(string serverShortName,
            IAirfieldRepository airfieldRepository,
            IPlayerRepository playerRepository,
            IUnitRepository unitRepository,
            Configuration.ControllerConfiguration controllerConfig,
            Configuration.SimpleRadioConfiguration simpleRadioConfig,
            ulong discordLogServerId,
            HashSet<AirfieldStatus> airfields,
            ResponderFactory responderFactory,
            MonitorFactory monitorFactory
            )
        {
            _serverShortName = serverShortName;
            _controllerConfig = controllerConfig;
            _simpleRadioConfig = simpleRadioConfig;
            _discordLogServerId = discordLogServerId;
            ControllerEnabled[Id] = controllerConfig.RespondsToTransmissions;

            QueueTransmissionDelegate queueTransmissionDelegate = QueueTransmission;
            LogTransmissionDelegate logTransmissionDelegate = LogBotTransmission;

            _responder = responderFactory.CreateResponder(controllerConfig.Type, playerRepository, unitRepository, airfieldRepository, airfields,
                controllerConfig.RestrictLineOfSight, controllerConfig.UnitPilot, controllerConfig.Callsign, _serverShortName);
            _monitor = monitorFactory.CreateMonitor(controllerConfig.Type, playerRepository, unitRepository, airfields, QueueTransmission, LogBotTransmission, controllerConfig.RestrictLineOfSight, controllerConfig.UnitPilot, _serverShortName);
        }

        public async Task RunAsync(CancellationToken gameServerCancellationToken)
        {
            using (MappedDiagnosticsLogicalContext.SetScoped("Controller", $"{_controllerConfig.Name}"))
            {
                _controllerCancellationTokenSource =
                    CancellationTokenSource.CreateLinkedTokenSource(gameServerCancellationToken);
                _logger.Info($"Starting RadioController on {_controllerConfig.Frequency:F3} {_controllerConfig.Modulation}");
                await ProcessingLoop();
            }
        }

        private async Task ProcessingLoop()
        {
            while (!_controllerCancellationTokenSource.IsCancellationRequested)
            {
                var controllerCancellationToken = _controllerCancellationTokenSource.Token;

                var processingTasks = new Dictionary<string, Task>();

                _client = new Client(_simpleRadioConfig.Host, _simpleRadioConfig.Port, _controllerConfig.Name,
                    _controllerConfig.Password,
                    _controllerConfig.Frequency, _controllerConfig.Modulation, _audioBuffer, _transmissionQueue, PlayersOnFrequency);

                var transmissionRecognizer = new TransmissionRecognizer(_audioBuffer, _controllerConfig.Name);

                processingTasks.Add("Network client", _client.ConnectAsync(controllerCancellationToken));
                processingTasks.Add("Speech Recognition trigger",
                    StartSpeechRecognitionOnAudio(transmissionRecognizer, controllerCancellationToken));
                processingTasks.Add("Monitor", _monitor.StartMonitoring(controllerCancellationToken));
                processingTasks.Add("ProcessReplies", ProcessReplies(controllerCancellationToken));

                _logger.Info("Started RadioController");

                await Task.WhenAll(processingTasks.Values);
                _logger.Info("Stopped RadioController");
            }
        }

        private async Task StartSpeechRecognitionOnAudio(TransmissionRecognizer transmissionRecognizer,
            CancellationToken controllerCancellationToken)
        {
            await Task.Run(async () =>
            {
                while (!controllerCancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        Thread.Sleep(50);
                        // First check to see if there is audio data that needs processing
                        if (_audioBuffer.BufferedBytes == 0)
                        {
                            _receivingTransmission = false;
                            continue;
                        }

                        _receivingTransmission = true;
                        TransmissionRecognizer.Result result = null;
                        try
                        {
                            result = transmissionRecognizer.RecognizeTransmissionAsync().Result;
                        }
                        catch (Exception ex)
                        {
                            var errorResponse = new RequestResponseModel
                            {
                                TransmissionException = ex
                            };
                            await LogRequestReplyTransmission(errorResponse);
                        }

                        if (result is not { IsSuccess: true }) continue;
                        _lastTransmission = DateTime.Now;
                        await ProcessTransmission(result, controllerCancellationToken);
                    }
                    catch (Exception ex)
                    {
                        _receivingTransmission = false;
                        _logger.Warn(ex);
                    }
                }
                if (!controllerCancellationToken.IsCancellationRequested)
                    _logger.Warn("Speech Recognition trigger stopped unexpectedly");
            }, CancellationToken.None);
        }

        private async Task ProcessTransmission(TransmissionRecognizer.Result recognitionResult,
            CancellationToken controllerCancellationToken)
        {

            await Task.Run(async () =>
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var requestResponse = new RequestResponseModel {RecognitionResult = recognitionResult};
                try
                {
                    // Understand the Transmission
                    requestResponse.Transmission = await TransmissionParser.ParseTransmission(requestResponse.RecognitionResult.Text);
                    if (requestResponse.Transmission.Intent == ITransmission.Intents.None) return;

                    if (!ControllerEnabled[Id]) return;

                    // Process the transmission
                    requestResponse.Reply = await _responder.ProcessTransmission(requestResponse.Transmission);
                    if (requestResponse.Reply.Details == null && requestResponse.Reply.Message == null) return;
                    QueueTransmission(requestResponse.Reply);
                    if (requestResponse.Reply.ContinueProcessing) await _monitor.ProcessTransmission(requestResponse.Transmission);
                }
                catch (Exception ex)
                {
                    requestResponse.TransmissionException = ex;
                    _logger.Warn(ex, $"Could not process transmission \"{requestResponse.RecognitionResult.Text}\"");
                    if (requestResponse.Transmission == null)
                    {
                        requestResponse.Transmission = new BasicTransmission(requestResponse.RecognitionResult.Text, ITransmission.Intents.None, new ITransmission.UnknownEntity(),
                            new ITransmission.UnknownEntity());
                        requestResponse.Reply = new Reply {ContinueProcessing = false};
                    }
                }
                finally
                {
                    watch.Stop();
                    requestResponse.ProcessingTime = watch.ElapsedMilliseconds;
                    await LogRequestReplyTransmission(requestResponse);
                }
            }, controllerCancellationToken);
        }
        
        private void QueueTransmission(Reply reply)
        {
            if (reply.Details == null && reply.Message == null)
                return;

            _messageQueue.Add(reply);
        }

        private async Task ProcessReplies(CancellationToken controllerCancellationToken)
        {
            while (!_controllerCancellationTokenSource.IsCancellationRequested)
            {
                await Task.Delay(50, controllerCancellationToken);

                if (_messageQueue.Count == 0) continue;

                var mergedCall =
                    _messageQueue.FirstOrDefault(reply => reply.Details is WarningRadiusChecker.MergedDetails);

                switch (_receivingTransmission)
                {
                    case true when mergedCall != null:
                    {
                        _messageQueue.Remove(mergedCall);
                        var urgentCall = await TransmissionSynthesizer.Instance
                            .Synthesize(SsmlWrapper.Wrap("break-break-break, " + mergedCall.ToSpeech(),
                                _controllerConfig.Voice));
                        _transmissionQueue.Enqueue(NetworkAudioEncoder.Encode(urgentCall.AudioData));
                        continue;
                    }
                    case true:
                        continue;
                }
                
                // This section should only be called when no one is transmitting and there are no urgent calls
                var reply = _messageQueue[0];
                _messageQueue.Remove(reply);
                
                // Create the response to the transmission
                var synthesis = await TransmissionSynthesizer.Instance
                    .Synthesize(SsmlWrapper.Wrap(reply.ToSpeech(), _controllerConfig.Voice));

                // Enqueue the response to the transmission
                _transmissionQueue.Enqueue(NetworkAudioEncoder.Encode(synthesis.AudioData));
            }
        }

        private async Task LogRequestReplyTransmission(RequestResponseModel requestResponse)
        {
            if (_discordLogServerId == 0 || _controllerConfig.DiscordLogChannel == 0) return;

            var actualResponse = requestResponse.Reply?.ToSpeech();
            if (!ControllerEnabled[Id])
            {
                actualResponse = "None (Controller Disabled)";
            }

            var transmissionLog = new RequestReplyTransmissionLog
            {
                DiscordServer = _discordLogServerId,
                DiscordChannel = _controllerConfig.DiscordLogChannel,
                ServerShortName = _serverShortName,
                ControllerName = _controllerConfig.Name,
                Frequency = _controllerConfig.Frequency,
                PlayersOnFrequency = PlayersOnFrequency.Values.ToList(),
                Modulation = _controllerConfig.Modulation,
                ControllerCallsign = _controllerConfig.Callsign,
                TransmissionDetails = requestResponse.Transmission?.ToDiscordLog() ??
                                      $"Request:\n\tText: {requestResponse.RecognitionResult?.Text}\n",
                Response = actualResponse ?? "None",
                ProcessingTime = requestResponse.ProcessingTime,
                Notes = requestResponse.Reply?.Notes,
                Exception = requestResponse.TransmissionException,
            };

            await Discord.Client.Instance.LogTransmission(transmissionLog);
        }

        private async Task LogBotTransmission(string text)
        {
            var transmissionLog = new BotTransmissionLog
            {
                DiscordServer = _discordLogServerId,
                DiscordChannel = _controllerConfig.DiscordLogChannel,
                ServerShortName = _serverShortName,
                ControllerName = _controllerConfig.Name,
                Frequency = _controllerConfig.Frequency,
                PlayersOnFrequency = PlayersOnFrequency.Values.ToList(),
                Modulation = _controllerConfig.Modulation,
                ControllerCallsign = _controllerConfig.Callsign,
                TransmissionText = text
            };

            await Discord.Client.Instance.LogTransmission(transmissionLog);
        }

        public ControllerStatus GetStatus()
        {
            return new ControllerStatus
            {
                Id = Id,
                ServerShortName = _serverShortName,
                ControllerName = _controllerConfig.Name,
                Frequency = _controllerConfig.Frequency,
                Modulation = _controllerConfig.Modulation,
                IsConnected = (_client != null) ? _client.IsConnected : false,
                LastTransmission = _lastTransmission,
                RespondsToTransmissions = ControllerEnabled[Id]
            };
        }
    }
}
