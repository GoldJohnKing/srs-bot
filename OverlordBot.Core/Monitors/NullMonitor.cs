﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading;
using System.Threading.Tasks;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;

#pragma warning disable 1998

namespace RurouniJones.OverlordBot.Core.Monitors
{
    public class NullMonitor : IMonitor
    {
        public async Task ProcessTransmission(ITransmission transmission)
        {
            // No-op
        }

        public async Task StartMonitoring(CancellationToken cancellationToken)
        {
            // No-op
        }
    }
}