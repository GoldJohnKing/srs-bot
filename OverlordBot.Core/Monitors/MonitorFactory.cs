﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Microsoft.Extensions.DependencyInjection;
using RurouniJones.OverlordBot.Datastore;
using System;
using System.Collections.Generic;

namespace RurouniJones.OverlordBot.Core.Monitors
{
    public class MonitorFactory
    {
        private readonly IServiceProvider _serviceProvider;
        public MonitorFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IMonitor CreateMonitor(Configuration.ControllerConfiguration.ControllerType type,
            IPlayerRepository playerRepository,
            IUnitRepository unitRepository,
            HashSet<AirfieldStatus> airfields,
            RadioController.QueueTransmissionDelegate queueTransmissionDelegate,
            RadioController.LogTransmissionDelegate logTransmissionDelegate,
            bool restrictedLineOfSight,
            string awacsPilot,
            string serverShortName
            )
        {
            if (type == Configuration.ControllerConfiguration.ControllerType.Awacs)
            {
                return ActivatorUtilities.CreateInstance<Awacs.AwacsMonitor>(_serviceProvider,
                    playerRepository,
                    unitRepository,
                    queueTransmissionDelegate,
                    logTransmissionDelegate,
                    restrictedLineOfSight,
                    awacsPilot,
                    serverShortName);
            }
            else if (type == Configuration.ControllerConfiguration.ControllerType.Atc)
            {
                return ActivatorUtilities.CreateInstance<Atc.AtcMonitor>(_serviceProvider,
                    airfields,
                    playerRepository,
                    queueTransmissionDelegate,
                    logTransmissionDelegate);
            }
            else
            {
                return _serviceProvider.GetService<NullMonitor>();
            }
        }
    }
}
