﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;

namespace RurouniJones.OverlordBot.Diagnostics
{
    public class ControllerStatus
    {
        public Guid Id { get; set; }
        public string ServerShortName { get; set; }
        public string ControllerName { get; set; }
        public double Frequency { get; set; }
        public string Modulation { get; set; }

        public bool IsConnected { get; set; }
        public DateTime LastTransmission { get; set; }
        public bool RespondsToTransmissions { get; set; }

        public string Initiator { get; set; }

        public string LastTransmissionAgo()
        {
            var diff = DateTime.Now - LastTransmission;

            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (diff.Days > 7)
            {
                return "No recent transmissions";
            }
            if (diff.Days > 0)
            {
                return $"Last transmission {diff.Days} days ago";
            }
            if (diff.Hours > 0)
            {
                return $"Last transmission {diff.Hours} hours ago";
            }
            if (diff.Minutes > 0)
            {
                return $"Last transmission {diff.Minutes} minutes ago";
            }
            // ReSharper disable once ConvertIfStatementToReturnStatement
            if (diff.Seconds > 0)
            {
                return $"Last transmission {diff.Seconds} seconds ago";
            }

            return null;
        }
    }
}
