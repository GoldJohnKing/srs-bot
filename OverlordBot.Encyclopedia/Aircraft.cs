﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace RurouniJones.OverlordBot.Encyclopedia
{
    public class Aircraft
    {
        private static readonly IDeserializer Deserializer = new DeserializerBuilder()
            .WithNamingConvention(UnderscoredNamingConvention.Instance)
            .Build();

        private static readonly HashSet<Aircraft> Entries = Deserializer.Deserialize<HashSet<Aircraft>>(
            File.ReadAllText("Data/Encyclopedia/Aircraft.yaml"));


        public string Name { get; set; }
        public string Code { get; set; }
        public List<string> DcsCodes { get; set; }
        public List<string> Attributes { get; set; }

        #region Get Codes

        public static List<string> GetCodesByIdentifier(string identifier)
        {
            return Entries.FirstOrDefault(x => x.Name.ToLower() == identifier ||
                                               x.Code.ToLower() == identifier ||
                                               x.DcsCodes.Contains(identifier, StringComparer.OrdinalIgnoreCase))?.DcsCodes;
        }

        public static List<string> GetCodesByAttributes(ISet<string> attributes)
        {
           return Entries.Where(e =>!attributes.Except(e.Attributes).Any()).SelectMany(x => x.DcsCodes).ToList();
        }

        #endregion

        public static string GetNameByDcsCode(string code)
        {
            var aircraft = Entries.FirstOrDefault(x => x.DcsCodes.Contains(code) || x.Code.Equals(code));
            return aircraft == null ? code : aircraft.Name;
        }

        public static string GetCodeByDcsCode(string code)
        {
            var aircraft = Entries.FirstOrDefault(x => x.DcsCodes.Contains(code));
            return aircraft == null ? code : aircraft.Code;
        }
    }
}
