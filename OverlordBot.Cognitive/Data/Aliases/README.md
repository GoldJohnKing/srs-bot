﻿# Entity Aliases

Despite our best efforts with voice training the bot is not always going to get
it right. These aliases allow us to provide a canonical representation of data
while also allowing alternative representations.

This will be particularly important with airfield names for non-english
airfields since the bot is *terrible* at understanding them, even with voice
training.

## Alias conflicts

It is possible with callsigns that someone asks for an alias to be added that
is then used by another player. e.g.

`harem` is often mishead by the bot as `heron` despite training so we add the
alias `heron` to resolve to `harem`. If someone then comes along and uses the
`heron` callsign it will not work because the bot resolves it and therefore
`heron` is out of luck.

In these situations the rule will be that we remove the alias. The player who
had the alias will have to do more voice training or choose another callsign.