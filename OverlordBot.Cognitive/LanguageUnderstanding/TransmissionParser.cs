﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime;
using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;

namespace RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding
{
    public class TransmissionParser
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly ITransmission.UnknownEntity UnknownEntity = new();

        public static Guid ApplicationId { private get; set; }
        public static string Endpoint { private get; set; }
        public static string ApiKey { private get; set; }
#if DEBUG
        private const bool LogUtterance = false;
#else
        private const bool LogUtterance = true;
#endif
        public static async Task<ITransmission> ParseTransmission(string inputText)
        {
            inputText = inputText.Replace('.', ' ');
            inputText = inputText.Replace(',', ' ');

            var runtimeClient = new LUISRuntimeClient(new ApiKeyServiceClientCredentials(ApiKey))
            {
                Endpoint = Endpoint
            };

            var predictionRequest = new PredictionRequest(inputText);
            var predictionResponse = await runtimeClient.Prediction.GetSlotPredictionAsync(ApplicationId, "production",
                predictionRequest, false, false, LogUtterance);
            return ParseResponse(predictionResponse);
        }

        public static ITransmission ParseResponse(PredictionResponse predictionResponse) {
            Logger.Info($"Prediction: {JsonConvert.SerializeObject(predictionResponse)}");

            ITransmission response;

            var request = predictionResponse.Query;
            var intentString = predictionResponse.Prediction.TopIntent;
            var intent = Enum.Parse<ITransmission.Intents>(intentString, true);
            if (intent == ITransmission.Intents.None)
            {
                return new BasicTransmission(request, ITransmission.Intents.None, UnknownEntity, UnknownEntity);
            }

            JToken intentEntity;
            // These are in all transmissions
            try
            {
                intentEntity = ((JArray) predictionResponse.Prediction.Entities[$"{intentString}Request"])[0];
            }
            catch (KeyNotFoundException)
            {
                return new BasicTransmission(request, intent, new ITransmission.UnknownEntity(), new ITransmission.UnknownEntity());
            }

            var intentDetails = new IntentDetails(intent, intentEntity);

            // ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
            switch (intentDetails.Intent)
            {
                case ITransmission.Intents.RadioCheck:
                case ITransmission.Intents.ReadyToTaxi:
                case ITransmission.Intents.InboundToAirfield:
                case ITransmission.Intents.AbortInbound:
                case ITransmission.Intents.CancelWarningRadius:
                case ITransmission.Intents.Picture:
                case ITransmission.Intents.Declare:
                    response = new BasicTransmission(request, intent, intentDetails.Transmitter, intentDetails.Receiver);
                    break;
                case ITransmission.Intents.BogeyDope:
                    var qualifier = SetQualifier((JArray) intentEntity["qualifier"]);
                    response = new BogeyDopeTransmission(request, intent, intentDetails.Transmitter, intentDetails.Receiver, qualifier);
                    break;
                case ITransmission.Intents.LocationOfEntity:
                    response = new LocationOfEntityTransmission(request, intent, intentDetails.Transmitter, intentDetails.Receiver,  intentDetails.Subject);
                    break;
                case ITransmission.Intents.SetWarningRadius:
                    var distance = SetDistance((JArray) intentEntity["warningRadius"]);
                    response = new SetWarningRadiusTransmission(request, intent, intentDetails.Transmitter, intentDetails.Receiver, distance);
                    break;
                case ITransmission.Intents.MissionAssignment:
                    response = new MissionAssignmentTransmission(request, intent, intentDetails.Transmitter, intentDetails.Receiver, intentDetails.MissionType);
                    break;
                case ITransmission.Intents.MissionJoin:
                    response = new MissionJoinTransmission(request, intent, intentDetails.Transmitter, intentDetails.Receiver, intentDetails.MissionId);
                    break;
                default:
                    response = new BasicTransmission(request, ITransmission.Intents.None, intentDetails.Transmitter, intentDetails.Receiver);
                    break;
            }
            return response;
        }

        private static ITransmission.IQualifier SetQualifier(JToken entity)
        {
            var tokens = entity?.SelectTokens("$..attribute[0]");
            if (tokens == null) return new ITransmission.Aircraft(null, null);

            var identifier = ((JArray) entity)[0]["identifier"]?[0]?.ToString().Replace(" ","-");
            var attributes = new JArray(tokens).ToObject<List<string>>()?.Select(EntityAliasResolver.ResolveAircraftAttribute).ToHashSet();

            if (attributes is { Count: 0 } || attributes.Contains("fighter"))
                return new ITransmission.Aircraft(identifier, attributes);

            // These attributes only work on conjunction with fighters. So if "fighter" is not also an attribute then remove these as well.
            attributes.Remove("short range");
            attributes.Remove("medium range");
            attributes.Remove("long range");

            return new ITransmission.Aircraft(identifier, attributes);
        }

        private static int SetDistance(JToken entity)
        {
            try
            {
                return Convert.ToInt32(((JValue) entity.First)?.Value);
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}