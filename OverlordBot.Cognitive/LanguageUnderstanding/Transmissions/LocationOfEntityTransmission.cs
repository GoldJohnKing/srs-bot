﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Text;

namespace RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions
{
    public class LocationOfEntityTransmission : ITransmission
    {
        public string Text { get; }
        public ITransmission.Intents Intent  { get; }
        public ITransmission.ITransmitter Transmitter  { get; }
        public ITransmission.IReceiver Receiver  { get; }
        public readonly ITransmission.ISubject Subject;
        public DateTime ReceivedAt { get; }

        public LocationOfEntityTransmission(string request, ITransmission.Intents intent, ITransmission.ITransmitter transmitter, ITransmission.IReceiver receiver, ITransmission.ISubject subject)
        {
            Text = request;
            Intent = intent;
            Transmitter = transmitter;
            Receiver = receiver;
            Subject = subject;
            ReceivedAt = DateTime.Now;
        }

        public override string ToString()
        {
            return $"Transmission: {Intent}, Request: {Text}, Transmitter: {Transmitter}, Receiver: {Receiver}, Subject {Subject}";
        }

        public string ToDiscordLog()
        {
            var transmitterType = "";
            var groupName = "_Unknown_";
            var flight = "_Unknown_";
            var element = "_Unknown_";

            if (Transmitter is ITransmission.Player transmitter)
            {
                transmitterType = transmitter.GetType().Name;
                if (transmitter.GroupName != null) groupName = transmitter.GroupName;
                if (transmitter.Flight >= 0) flight = transmitter.Flight.ToString();
                if (transmitter.Element >= 0) element = transmitter.Element.ToString();
            }

            var sb = new StringBuilder()
                .AppendLine("Request:")
                .AppendLine($"\tText: {Text}")
                .AppendLine($"\tTransmitter: {transmitterType}")
                .AppendLine($"\t\tGroup Name: {groupName}")
                .AppendLine($"\t\tFlight Number: {flight}")
                .AppendLine($"\t\tElement Number: {element}")
                .AppendLine($"\tReceiver: {Receiver?.GetType().Name ?? "_Unknown_"}")
                .AppendLine($"\t\tCallsign: {Receiver?.Callsign ?? "_Unknown_"}")
                .AppendLine($"\tIntent: {Intent}")
                .AppendLine($"\tSubject: {Subject?.GetType().Name ?? "_Unknown_"}");

            switch (Subject?.GetType().Name)
            {
                case "Airfield":
                    sb.AppendLine($"\t\tName: {((ITransmission.Airfield) Subject).Name}");
                    break;
                case "Player":
                {
                    var subject = (ITransmission.Player) Subject;
                    var subjectGroupName = "_Unknown_";
                    var subjectFlight = "_Unknown_";
                    var subjectElement = "_Unknown_";

                    if (subject.GroupName != null) subjectGroupName = subject.GroupName;
                    if (subject.Flight >= 0) subjectFlight = subject.Flight.ToString();
                    if (subject.Element >= 0) subjectElement = subject.Element.ToString();

                    sb.AppendLine($"\t\tGroup Name: {subjectGroupName}")
                        .AppendLine($"\t\tFlight Number: {subjectFlight}")
                        .AppendLine($"\t\tElement Number: {subjectElement}");
                    break;
                }
            }

            return sb.ToString();
        }
    }
}
